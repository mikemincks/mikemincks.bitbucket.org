<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main">

  <div class="w3-row-padding w3-padding-16" id="sdc">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="uk-table uk-table-divider uk-table-small">
        <thead>
          <tr>
            <th colspan="2">SDC</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>SDC Main</td>
            <td>888-434-0055</td>
          </tr>
          <tr>
            <td>GAP</td>
            <td>888-899-1255</td>
          </tr>
          <tr>
            <td>CR</td>
            <td>424-225-8791</td>
          </tr>
          <tr>
            <td>Enterprise</td>
            <td>877-782-6785</td>
          </tr>
          <tr>
            <td>HVST</td>
            <td>877-782-6739</td>
          </tr>
          <tr>
            <td>Payment Collections</td>
            <td>844-903-2428</td>
          </tr>
          <tr>
            <td>Tech</td>
            <td>424-225-8793</td>
          </tr>
          <tr>
            <td>Tech Tier 2</td>
            <td>650-543-6450</td>
          </tr>
          <tr>
            <td>Security</td>
            <td>866-634-4708</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="sdcC">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="uk-table uk-table-divider uk-table-small">
        <thead>
          <tr>
            <th colspan="2">SDC Companies</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Endicia</td>
            <td>800-576-3279</td>
          </tr>
          <tr>
            <td>Endicia Tech</td>
            <td>650-321-2640</td>
          </tr>
          <tr>
            <td>ShipStation</td>
            <td>512-485-4282</td>
          </tr>
          <tr>
            <td>Shipping Easy</td>
            <td>855-202-2275</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="mail">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="uk-table uk-table-divider uk-table-small">
        <thead>
          <tr>
            <th colspan="2">Mail Services</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Canada Post</td>
            <td>866-607-6301</td>
          </tr>
          <tr>
            <td>EDC/SDC USPS Rep</td>
            <td>855-877-7411</td>
          </tr>
          <tr>
            <td>USPS Customer Service</td>
            <td>800-222-1811</td>
          </tr>
          <tr>
            <td>USPS Small Business</td>
            <td>877-747-6249</td>
          </tr>
          <tr>
            <td>USPS Supplies</td>
            <td>800-610-8734</td>
          </tr>
          <tr>
            <td>USPS Tracking</td>
            <td>800-275-8777</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="others">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="uk-table uk-table-divider uk-table-small">
        <thead>
          <tr>
            <th colspan="2">Other Services</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>U-Pic</td>
            <td>800-955-4623</td>
          </tr>
          <tr>
            <td>White Label</td>
            <td>877-522-8510</td>
          </tr>
          <tr>
            <td>PIP</td>
            <td>877-262-1161</td>
          </tr>
          <tr>
            <td>CARS / CAP</td>
            <td>800-877-7435 or 310-482-5810</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="address">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="uk-table uk-table-divider uk-table-small">
        <thead>
          <tr>
            <th colspan="2">Addresses</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>EDC Main</td>
            <td>278 Castro St Mountain View, CA 94041-1204</td>
          </tr>
          <tr>
            <td>SDC Main</td>
            <td>1990 E Grand Ave El Segundo, CA 90245-5013</td>
          </tr>
          <tr>
            <td>SDC Misprints</td>
            <td>PO Box 6026 Inglewood CA 90312-6026</td>
          </tr>
          <tr>
            <td>SDC Returns</td>
            <td>Attn : Order # 28355 Witherspoon Pkwy Valencia CA 91355-4178</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>


  <!-- END MAIN -->
</div>