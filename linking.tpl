<!-- Sidebar -->
<nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5 w3-animate-left" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()"
    class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
    <i class="fa fa-remove"></i>
  </a>
  <div class="w3-bar-block">
    <a href="#shipstation" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fa fa-search fa-fw w3-margin-right"></i>Ship Station</a>
    <a href="#shippingeasy" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fa fa-search fa-fw w3-margin-right"></i>Shipping Easy</a>
  </div>
</nav>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu"
  id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main">

  <div class="w3-row-padding w3-padding-16" id="shipstation">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Shipstation</h3>
        </header>

        <div class="w3-container">
          <ol>
            <li>Settings (Gear Icon) > Shipping > Carriers & Fulfillment</li>
            <li>Click the Gear Icon on the  right hand side of USPS by Stamps.com.</li>
            <li>Click the Update Password button then enter your new Stamps.com password.</li>
            <li>Click Confirm when you are done.</li>
          </ol>
        </div>
      </div>

    </div>
  </div>


<div class="w3-row-padding w3-padding-16" id="shippingeasy">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Shipping Easy</h3>
        </header>

        <div class="w3-container">
          <ol>
            <li>From the top right corner in ShippingEasy, navigate to the ONE BALANCE AND CARRIERS page.</li>
            <li>Click the Manage Account link for the One Balance account.</li>
            <li>Click the green "Reconnect" button then enter your new Stamps.com password.</li>
            <li>Click Confirm when you are done.</li>
          </ol>
        </div>
      </div>

    </div>
  </div>

  <!-- END MAIN -->
</div>