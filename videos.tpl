<!-- Sidebar -->
<nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5 w3-animate-left" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()"
    class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
    <i class="fa fa-remove"></i>
  </a>
  <div class="w3-bar-block">
    <a href="#adminHold" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Admin Hold - Closures / Refund</a>
    <a href="#apv" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>APV Adjustments</a>
    <a href="#batch" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Batch</a>
    <a href="#bridgeAccounts" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Bridge Accounts</a>
    <a href="#certifiedMail" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Certified Mail</a>
    <a href="#connect" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Connect</a>
    <a href="#connectivity" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Connectivity</a>
    <a href="#enableNet" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Enable .Net Framework</a>
    <a href="#e4m" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Endicia For Mac</a>
    <a href="#error404" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Error 404</a>
    <a href="#negotiationRefunds	" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Negotiation / Refunds</a>
    <a href="#printer" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Printer Tech</a>
    <a href="#red" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>RED</a>
    <a href="#reprinting" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Reprinting</a>
    <a href="#resub" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Resubmission</a>
    <a href="#sas" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>SAS Cancelation (CC Refund/Check Request)</a>
    <a href="#sdcSoftware" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i  class="fas fa-file-video fa-fw w3-margin-right"></i>SDC Software</a>
    <a href="#securePause" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Secure Pause</a>
    <a href="#statementsReports" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Statements & Reports</a>
    <a href="#transferLines" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Transfer Lines</a>
    <a href="#understandingAPV" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Understanding APV's</a>
    <a href="#webclient" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fas fa-file-video fa-fw w3-margin-right"></i>Web Client</a>
  </div>
</nav>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu"
  id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main">

  <div class="w3-row-padding w3-padding-16" id="adminHold">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Admin Hold - Closures / Refund</h3>
        </header>

        <div class="w3-container">

          <video width="320" height="240" controls>
            <source src="http://internallinks/Agent/Stamps%20Videos/Admin_Hold_Closures_Refund.mp4" type="video/mp4">
            Your browser does not support the video tag.
          </video>

        </div>
      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="apv">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>APV Adjustments</h3>
        </header>

        <div class="w3-container">

          <video width="320" height="240" controls>
            <source src="http://internallinks/Agent/Stamps%20Videos/APV%20Adjustments.mp4" type="video/mp4">
            Your browser does not support the video tag.
          </video>

        </div>
      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="batch">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Batch</h3>
        </header>

        <div class="w3-container">

          <video width="320" height="240" controls>
            <source src="http://internallinks/Agent/Stamps%20Videos/Batch.mp4" type="video/mp4">
            Your browser does not support the video tag.
          </video>

        </div>
      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="bridgeAccounts">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Bridge Accounts</h3>
        </header>

        <div class="w3-container">

          <video width="320" height="240" controls>
            <source src="http://internallinks/Agent/Stamps%20Videos/Bridge_Accounts.mp4" type="video/mp4">
            Your browser does not support the video tag.
          </video>

        </div>
      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="certifiedMail">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Certified Mail</h3>
        </header>

        <div class="w3-container">

          <video width="320" height="240" controls>
            <source src="http://internallinks/Agent/Stamps%20Videos/Certified%20Mail.mp4" type="video/mp4">
            Your browser does not support the video tag.
          </video>

        </div>
      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="connect">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Connect</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/SDC%20Connect.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="connectivity">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Connectivity</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Connectivity.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="enableNet">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Enable .Net Framework</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Enable%20.Net%20Framework%20(tech).mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="e4m">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Endicia For Mac</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Endicia%20For%20Mac.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="error404">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Error 404</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/ERROR%20404.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="negotiationRefunds">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Negotiation / Refunds</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Negotiation%20Refunds.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="printer">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Printer Tech</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Printers.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="red">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>RED</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/RED.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="reprinting">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Reprinting</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Reprinting.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="resub">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Resubmission/h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Credit%20Cards%20-%20Resubmission.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="sas">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>SAS Cancelation (CC Refund/Check Request)</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/SAS%20CANC-%20CC%20RefundCheck%20Request.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="sdcSoftware">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>SDC Software</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/SDC%20Software.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="securePause">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Secure Pause</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Secure%20Pause.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="statementsReports">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Statements & Reports</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Statements%20&%20Reports.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="transferLines">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Transfer Lines</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Transfer%20Lines.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="understandingAPV">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Understanding APV's</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/Understanding%20APV.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="webclient">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Web Client</h3>
        </header>
        
        <video width="320" height="240" controls>
          <source src="http://internallinks/Agent/Stamps%20Videos/WebClient.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>

      </div>

    </div>
  </div>


  <!-- END MAIN -->
</div>