<!-- Sidebar -->
<nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5 w3-animate-left" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()"
    class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
    <i class="fa fa-remove"></i>
  </a>
  <div class="w3-bar-block">
    <a href="#sdcSoftware" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fa fa-search fa-fw w3-margin-right"></i>Stamps Software</a>
    <a href="#sdcold" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fa fa-search fa-fw w3-margin-right"></i>Stamps.com Older Versions</a>
    <a href="#sdcold" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fa fa-search fa-fw w3-margin-right"></i>Endicia Older Versions</a>
    <a href="#edcSoftware" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fa fa-search fa-fw w3-margin-right"></i>Endicia Software</a>
    <a href="#printer" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fa fa-print fa-fw w3-margin-right"></i>Printer Drivers</a>
    <a href="#microsoft" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fab fa-microsoft fa-fw w3-margin-right"></i>Microsoft Resources</a>
  </div>
</nav>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu"
  id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main">

  <div class="w3-row-padding w3-padding-16" id="sdcSoftware">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="w3-panel w3-card w3-table-all">
        <thead>
          <tr>
            <th colspan="2">Stamps Software</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>SDC Latest (32 Bit)</td>
            <td>https://www.stamps.com/site/resources/stamps.exe</td>
          </tr>
          <tr>
            <td>SDC Latest (64 Bit)</td>
            <td>https://www.stamps.com/site/resources/stamps64.exe</td>
          </tr>
          <tr>
            <td>Clean</td>
            <td>https://support.stamps.com/outgoing/clean.exe</td>
          </tr>
          <tr>
            <td>Key</td>
            <td>https://support.stamps.com/outgoing/key.exe</td>
          </tr>
          <tr>
              <td>Test</td>
              <td>https://support.stamps.com/outgoing/test-stamps.exe</td>
          </tr>
          <tr>
            <td>WICU</td>
            <td>https://support.stamps.com/outgoing/wicu.exe</td>
          </tr>
          <tr>
            <td>SDC TeamViewer for PC</td>
            <td>https://support.stamps.com/outgoing/remote.exe</td>
          </tr>
          <tr>
            <td>SDC TeamViewer for MAC</td>
            <td>https://support.stamps.com/outgoing/remotemac.zip</td>
          </tr>
          <tr>
            <td>SDC Connect for Windows</td>
            <td>https://print.stamps.com/Webpostage/resources_connect_download/StampsConnect.exe</td>
          </tr>
          <tr>
            <td>SDC Connect for MAC</td>
            <td>https://print.stamps.com/Webpostage/resources_connect_download/StampsConnect.pkg</td>
          </tr>

        </tbody>
      </table>
    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="edcSoftware">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="w3-panel w3-card w3-table-all">
        <thead>
          <tr>
            <th colspan="2">Endicia Software</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>EDC Connect for Windows</td>
            <td>https://print.endicia.com/Webpostage/resources_connect_download/EndiciaConnect.exe</td>
          </tr>
          <tr>
            <td>EDC Connect for MAC</td>
            <td>https://print.endicia.com/Webpostage/resources_connect_download/EndiciaConnect.pkg</td>
          </tr>
          <tr>
            <td>DAZzle-SDC (21.1.xx) Full</td>
            <td>http://download.endicia.com/DAZzle-Setup-S.EXE</td>
          </tr>
          <tr>
            <td>DAZzle-SDC (21.1.xx) Update</td>
            <td>http://download.endicia.com/DAZzle-Update-S.EXE</td>
          </tr>
          <tr>
            <td>Endicia Professional SDC (6.32.xx)</td>
            <td>http://download.endicia.com/EndiciaProfessionalFullSetup.exe</td>
          </tr>
          <tr>
            <td>Endicia for Mac SDC (3.xx)</td>
            <td>http://mac.endicia.com/upgrade/</td>
          </tr>
          <tr>
            <td>Endicia Platinum Shipper</td>
            <td>http://download.endicia.com/PlatinumShipperInstall.exe</td>
          </tr>
          <tr>
            <td>Endicia Professional for MOM</td>
            <td>http://download.endicia.com/EndiciaProMomSetup.exe</td>
          </tr>
          <tr>
            <td>Endicia Professional for ShopGoodwill</td>
            <td>http://download.endicia.com/EndiciaProSGWsetup.exe</td>
          </tr>
          <tr>
            <td>Endicia Professional ScriptPro Plugin</td>
            <td>http://download.endicia.com/EndiciaProScriptProPluginSetup.exe</td>
          </tr>
          <tr>
            <td>ShipTicket Plugin</td>
            <td>http://download.endicia.com/EndiciaProShipTicketPluginSetup.exe</td>
          </tr>
          <tr>
            <th colspan="2">Depreciated (Old)</th>
          </tr>
          <tr>
            <td>Endicia for Mac EDC (2.xx)</td>
            <td>http://download.endicia.com/EndiciaForMac.dmg</td>
          </tr>
           <tr>
            <td>DAZzle-EDC (18.1.xx) Full</td>
            <td>http://download.endicia.com/DAZzle-Setup-E.EXE</td>
          </tr>
          <tr>
            <td>DAZzle-EDC (18.1.xx) Update</td>
            <td>http://download.endicia.com/DAZzle-Update-E.EXE</td>
          </tr>
          <tr>
            <td>Endicia Professional EDC (6.29.xx)</td>
            <td>http://download.endicia.com/EndiciaProSetup.exe</td>
          </tr>
          <tr>
            <td>Endicia Platinum Shipper (Latest version, exe only)</td>
            <td>
              https://www.ozdevelopment.com/support/downloads/root/Endicia/2020/PlatinumShipper.20.8.0.431.ApplicationOnly.zip
            </td>
          </tr>
          <tr>
            <td>DYMO Printable Postage (Windows)</td>
            <td>http://download.endicia.com/PrintablePostage/PrintablePostageSetup.exe</td>
          </tr>
          <tr>
            <td>DYMO Printable Postage (Mac)</td>
            <td>http://download.endicia.com/PrintablePostage/PrintablePostage.dmg</td>
          </tr>
          <tr>
            <td>DYMO Stamps (Windows)</td>
            <td>http://download.endicia.com/DYMOstamps/DYMOstampsWebSetup.exe</td>
          </tr>
          <tr>
            <td>DYMO Stamps (Mac)</td>
            <td>http://download.endicia.com/dymostamps/dymostamps.dmg</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="printer">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="w3-panel w3-card w3-table-all">
        <thead>
          <tr>
            <th colspan="2">Printer Drivers</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Zebra Seagull</td>
            <td>http://downloads.seagullscientific.com/drivers/archive/2020/2020.3/Zebra_2020.3.exe</td>
          </tr>
          <tr>
            <td>Zebra Universal</td>
            <td>https://support.stamps.com/outgoing/zud.exe</td>
          </tr>
          <tr>
            <td>P2 Driver</td>
            <td>https://support.stamps.com/outgoing/P2printer.zip</td>
          </tr>
          <tr>
            <td>P2 Bios</td>
            <td>https://support.stamps.com/outgoing/ProBIOSv2_190517_3KDSTX_STP2.zip</td>
          </tr>
          <tr>
            <td>ProLabel+ Driver</td>
            <td>https://support.stamps.com/outgoing/stampsprolabelprinterdriver.zip</td>
          </tr>
          <tr>
            <td>P1 Driver</td>
            <td>https://support.stamps.com/outgoing/p1driver.exe</td>
          </tr>
          <tr>
            <td>Pro Label Express</td>
            <td>http://support.stamps.com/outgoing/tscdriver.exe</td>
          <tr>
          <tr>
            <td>Pro Label Titan</td>
            <td>http://store.stamps.com/web/downloads/TSC_7.4.2_M-2.zip</td>
          </tr>
          <tr>
            <td>Dymo (x64)</td>
            <td>
              http://download.dymo.com/dymo/Software/Download%20Drivers/LabelWriter/Downloads/1/LabelWriterDrivers-x64.msi
            </td>
          </tr>
          <tr>
            <td>Dymo (x86)</td>
            <td>
              http://download.dymo.com/dymo/Software/Download%20Drivers/LabelWriter/Downloads/1/LabelWriterDrivers-x86.msi
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="microsoft">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="w3-panel w3-card w3-table-all">
        <thead>
          <tr>
            <th colspan="2">Microsoft Resources</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>C++ 2012 Redistributable</td>
            <td>https://www.microsoft.com/en-us/download/details.aspx?id=30679</td>
          </tr>
          <tr>
            <td>C++ 2013 Redistributable</td>
            <td>https://www.microsoft.com/en-us/download/details.aspx?id=40784</td>
          </tr>
          <tr>
            <td>.Net 3.5 Full</td>
            <td>https://www.microsoft.com/en-us/download/details.aspx?id=21</td>
          </tr>
          <tr>
            <td>Program Install and Uninstall troubleshooter</td>
            <td>https://support.microsoft.com/en-us/help/17588/windows-fix-problems-that-block-programs-being-installed-or-removed</td>
          </tr>
          <tr>
            <td>64 Bit ODC Driver (Excel, CSV's)</td>
            <td>
              https://www.microsoft.com/en-us/download/confirmation.aspx?id=13255&6B49FDFB-8E5B-4B07-BC31-15695C5A2143=1
            </td>
          </tr>
          <tr>
            <td>64 Bit ODC Driver (Excel, CSV's) (SDC Link)</td>
            <td>https://support.stamps.com/outgoing/accessdatabaseengine.exe</td>
          </tr>

        </tbody>
      </table>
    </div>
  </div>

<div class="w3-row-padding w3-padding-16" id="edcold">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="w3-panel w3-card w3-table-all">
        <thead>
          <tr>
            <th colspan="2">Endicia Old Verisons</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Dazzle 20.06.02</td>
            <td>http://download.endicia.com/DAZzle-Setup-20602.EXE</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>


  <div class="w3-row-padding w3-padding-16" id="sdcold">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">
      <table class="w3-panel w3-card w3-table-all">
        <thead>
          <tr>
            <th colspan="2">Stamps.com Old Verisons</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>SDC 17.5</td>
            <td>http://support.stamps.com/outgoing/stamps175.exe</td>
          </tr>
          <tr>
            <td>SDC 17.4</td>
            <td>http://support.stamps.com/outgoing/stamps174.exe</td>
          </tr>
          <tr>
            <td>SDC 17.3</td>
            <td>http://support.stamps.com/outgoing/stamps173.exe</td>
          </tr>
          <tr>
            <td>SDC 17.1</td>
            <td>http://support.stamps.com/outgoing/stamps171.exe</td>
          </tr>
          <tr>
            <td>SDC 16.3</td>
            <td>http://support.stamps.com/outgoing/stamps163.exe</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <!-- END MAIN -->
</div>