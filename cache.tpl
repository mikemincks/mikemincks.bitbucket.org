<!-- Sidebar -->
<nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5 w3-animate-left" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()"
    class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
    <i class="fa fa-remove"></i>
  </a>
  <div class="w3-bar-block">
    <a href="#chrome" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fab fa-chrome fa-fw w3-margin-right"></i>Chrome</a>
    <a href="#edge" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fab fa-edge fa-fw w3-margin-right"></i>Edge</a>
    <a href="#ie" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fab fa-internet-explorer fa-fw w3-margin-right"></i>Internet Explorer</a>
    <a href="#firefox" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fab fa-firefox fa-fw w3-margin-right"></i>Firefox</a>
    <a href="#Safari" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i
        class="fab fa-safari fa-fw w3-margin-right"></i>Safari</a>
  </div>
</nav>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu"
  id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main">

  <div class="w3-row-padding w3-padding-16" id="chrome">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Chrome</h3>
        </header>

        <div class="w3-container">
          <ol>
            <li>Click the Menu Button (3 Vertical Dots)</li>
            <li>Click More tools > Clear browsing data</li>
            <li>Under Time Rage, Select "All Time"</li>
            <li>Next we select "Cookies and other site data" and "Cached images and files," check the boxes</li>
            <li>Click Clear data</li>
          </ol>
        </div>
      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="edge">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">


      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Edge (Chromium Based)</h3>
        </header>

        <div class="w3-container">
          <ol>
            <li>Click the Menu Button (3 Horizontal Dots)</li>
            <li>Click Settings</li>
            <li>On the left hand side select "Privacy, search, and services"</li>
            <li>Scroll down to "Clear browsing data"</li>
            <li>Under Time Rage, Select "All Time"</li>
            <li>Next we select "Cookies and other site data" and "Cached images and files," check the boxes</li>
            <li>Click "Clear Now"</li>
          </ol>
        </div>
      </div>

    </div>
  </div>


  <div class="w3-row-padding w3-padding-16" id="ie">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">


      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Internet Explorer</h3>
        </header>

        <div class="w3-container">
          <ol>
            <li>Click the Settings icon (Little Gear)</li>
            <li>Click Internet Options</li>
            <li>Under Browsing history, Click Delete</li>
            <li>Next to "Preservce Favorites website data", make sure it is unchecked</li>
            <li>Next to "Temporary Internet files and website files" and "Cookies and website data," check the boxes
            </li>
            <li>Click Delete</li>
            <li>Restart Browser</li>
          </ol>
        </div>
      </div>

    </div>
  </div>


  <div class="w3-row-padding w3-padding-16" id="firefox">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Firefox</h3>
        </header>

        <div class="w3-container">
          <ol>
            <li>Click the settings (humburger) button on the top-right corner</li>
            <li>Click on Options</li>
            <li>Click on Privacy</li>
            <li>Under the “Cookies and Site Data” section, click the Clear Data button</li>
            <li>Click Clear</li>
          </ol>
        </div>
      </div>

    </div>
  </div>

  <div class="w3-row-padding w3-padding-16" id="safari">
    <div class="w3-container w3-padding-large" style="margin-bottom:32px">

      <div class="w3-card-4 w3-white">

        <header class="w3-container">
          <h3>Safari</h3>
        </header>

        <div class="w3-container">
          <ol>
            <li>Click on Safari (at top left corner of browser) in the menu bar</li>
            <li>Click on Prefences</li>
            <li>Click on the Privacy Tab</li>
            <li>Click the "Manage Website Data" button</li>
            <li>Select Remove All and click on Remove now in the popup.</li>
            <strong>To clear cache</strong>
            <li>Check in Advanced tab, if Show Develop menu in menu bar is selected. If not, Check the box</li>
            <li>Select Develop in the menu bar on the top of the screen and click Empty Caches to clear Cache</li>
          </ol>
        </div>
      </div>

    </div>
  </div>


  <!-- END MAIN -->
</div>